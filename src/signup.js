import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';
function Signup() {

  return (
    <div>
    <h1 textcolor="blue">Signup</h1>
    <p>Please fill the form to create an account.</p>
    <Box
      component="form"
      sx={{
        '& > :not(style)': { m: 0.5, width: '25ch' },
      }}
      noValidate
      autoComplete="off"
    >
      <TextField type="text" id="outlined-basic" label="Name" variant="outlined" /><br />
      <TextField type="email" id="outlined-basic" label="Email" variant="outlined" /><br />
      <TextField type="number" id="outlined-basic" label="Mobile" variant="outlined" /><br />
      <TextField type="password" id="outlined-basic" label="Password" variant="outlined" /><br />
      <TextField type="password" id="outlined-basic" label="Repeat-Password" variant="outlined" /><br />
    </Box><br />
    <Button variant="contained">Submit</Button>
      <p>Already have an account? <Link to="/signin">SignIn</Link></p>
    </div>
    
  );
}
export default Signup;