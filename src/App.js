import Signup from './signup';
import './App.css';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import React from 'react';
import Home from './signin';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
      <Routes>
      <Route exact path="/" element={<Home />} />
          <Route exact path="/signup" element={<Signup />} />
          <Route exact path="/signin" element={<Home />} />


        </Routes>
     
    </div>
    </BrowserRouter>
    
  );
}

export default App;
