import {
  Button,
  FormControlLabel,
  Grid,
  Paper,
  TextField,
} from "@mui/material";
import Checkbox from "@mui/material/Checkbox";
import { Link } from "react-router-dom";

import React from "react";

function Home() {
  var paperStyle = {
    padding: 10,
    height: "78vh",
    width: 288,
    margin: "20px auto",
  };
  var btnstyle = { margin: "8px 0" };

  

  return (
    <Grid>
      <Paper elevation={10} style={paperStyle}>
        <h2> Sign In </h2>
        <TextField
          label="E-Mail"
          placeholder="Enter your E-Mail"
          required
          fullWidth
        />{" "}
        <br /> <br />
        <TextField
          label="Password"
          placeholder="Enter your Password"
          type={"password"}
          required
          fullWidth
        />{" "}
        <br />
        <FormControlLabel
          control={<Checkbox value="remember" color="primary" />}
          label="Remember me"
        />
        <br />
        <Button type="Submit" variant="contained" style={btnstyle} fullWidth>
          Sign In
        </Button>{" "}
        <br />
        <Link to="#">Forgot password ?</Link>
        <br /><br />
        <div>
          Don't you have an account ?<Link to= "/signup">SignUp</Link>
          <br />
        </div>
      </Paper>
    </Grid>
  );
}
export default Home;
